@extends('layout.welcome')

@section('title')
    Manage Tugas
@endsection

@section('content')
    @php
        use Carbon\Carbon;
    @endphp
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center">
                            <h5 class="card-title">{{ $tugas->title }}</h5>
                            <div class="dropdown">
                                <button type="button" class="btn btn-outline-secondary btn-rounded btn-icon" type="button"
                                    id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">
                                    <i class="mdi mdi-dots-horizontal"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <button type="button" class="dropdown-item" data-toggle="modal"
                                        data-target="#editTugas">
                                        Edit Tugas
                                    </button>
                                    <button type="button" class="dropdown-item" data-toggle="modal"
                                        data-target="#hapusTugas">
                                        Hapus Tugas
                                    </button>
                                </div>
                            </div>
                        </div>
                        <p class="font-weight-500">{{ $tugas->deskripsi }}</p>
                        @if ($tugas->path_file != '')
                            <p class="font-weight-500"><a href="{{ asset('storage/tugas_files/' . $tugas->path_file) }}"
                                    target="_blank">Klik Disini</a> untuk mendownload
                                soal</p>
                        @else
                            <p class="font-weight-500">Kerjakanlah Soal Dibawah Ini</p>
                            {!! $tugas->soal !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">List Pengumpulan Tugas Siswa</h5>
                        <div class="table-responsive">
                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Siswa</th>
                                        <th>Status Pengerjaan</th>
                                        <th>Waktu Pengumpulan Tugas</th>
                                        <th>Nilai</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($user as $no => $item)
                                        <tr>
                                            <td>{{ $no + 1 }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>
                                                @if ($pengumpulan->where('user_id', $item->id)->where('tugas_id', $tugas->id)->first() != null)
                                                    <div class="badge badge-success">Sudah Mengumpulkan</div>
                                                @else
                                                    <div class="badge badge-danger">Belum Mengumpulkan</div>
                                                @endif
                                            </td>
                                            <td>
                                                @if ($pengumpulan->where('user_id', $item->id)->where('tugas_id', $tugas->id)->first() != '')
                                                    {{ $pengumpulan->where('user_id', $item->id)->where('tugas_id', $tugas->id)->first()->created_at }}
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td>
                                                @if ($pengumpulan->where('user_id', $item->id)->where('tugas_id', $tugas->id)->first() != '')
                                                    {{ $pengumpulan->where('user_id', $item->id)->where('tugas_id', $tugas->id)->first()->nilai }}
                                                @else
                                                    -
                                                @endif
                                            </td>
                                            <td class="text-center">
                                                @if ($pengumpulan->where('user_id', $item->id)->where('tugas_id', $tugas->id)->first() != '')
                                                    <a href="/teacher/detail-pengumpulan/{{ $pengumpulan->where('user_id', $item->id)->where('tugas_id', $tugas->id)->first()->id }}"
                                                        class="btn btn-info {{ $item->id ? '' : 'disabled' }}"><i
                                                            class="mdi mdi-eye"></i></a>
                                                @else
                                                    <a href="#"
                                                        class="btn btn-info {{ !isset($pengumpulan->where('user_id', $item->id)->where('tugas_id', $tugas->id)->first()->id)? 'disabled': '' }}"><i
                                                            class="mdi mdi-eye"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Siswa</th>
                                        <th>Status Pengerjaan</th>
                                        <th>Waktu Pengumpulan Tugas</th>
                                        <th>Nilai</th>
                                        <th>Aksi</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->

    <div class="modal fade" id="editTugas" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update Tugas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/teacher/update-tugas/{{ $tugas->id }}" method="post" class="forms-sample"
                        enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label for="title">Judul Tugas</label>
                            <input type="text" class="form-control @error('title') is-invalid @enderror" id="title"
                                placeholder="Masukkan Judul Topik Materi" name="title" value="{{ $tugas->title }}">
                            @error('title')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="deskripsi">Deskripsi Singkat Tugas (anda bisa memberikan soal disini)</label>
                            <textarea class="form-control @error('deskripsi') is-invalid @enderror" id="deskripsi" rows="4" name="deskripsi"
                                placeholder="Masukkan Deskripsi Topik Materi">{{ $tugas->deskripsi }}</textarea>
                            @error('deskripsi')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="deadline_tanggal">Deadline Tugas (Hari & Tanggal)</label>
                            <input type="date" name="deadline_tanggal" id="deadline_tanggal"
                                class="form-control @error('deadline_tanggal') is-invalid @enderror"
                                value="{{ $tugas->deadline_tanggal }}">
                            @error('deadline_tanggal')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="deadline_jam">Deadline Tugas (Jam)</label>
                            <input type="time" name="deadline_jam" id="deadline_jam"
                                class="form-control @error('deadline_jam') is-invalid @enderror"
                                value="{{ $tugas->deadline_jam }}">
                            @error('deadline_jam')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="home-tab" data-toggle="tab"
                                    data-target="#jawab_file" type="button" role="tab" aria-controls="home"
                                    aria-selected="true">Soal
                                    Bentuk File</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="profile-tab" data-toggle="tab" data-target="#jawab_manual"
                                    type="button" role="tab" aria-controls="profile" aria-selected="false">Ketik
                                    Soal
                                    Manual</button>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="jawab_file" role="tabpanel"
                                aria-labelledby="home-tab">
                                <div class="form-group">
                                    <label>Upload File Soal</label>
                                    <input type="file" name="file" id="file"
                                        class="file-upload-default @error('file') is-invalid @enderror">
                                    <div class="input-group col-xs-12">
                                        <input type="text" class="form-control file-upload-info" disabled
                                            placeholder="Upload File Tugas">
                                        <span class="input-group-append">
                                            <button class="file-upload-browse btn btn-primary"
                                                type="button">Upload</button>
                                        </span>
                                    </div>
                                    @error('file')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="tab-pane fade" id="jawab_manual" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="form-group">
                                    <label for="soal">Silahkan Buat Soal Pada Kolom Di Bawah</label>
                                    <textarea class="form-control @error('soal') is-invalid @enderror" id="soal" rows="4" name="soal">{{ $tugas->soal }}</textarea>
                                    @error('soal')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update Topik Diskusi</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="hapusTugas" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Hapus Tugas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/teacher/delete-tugas/{{ $tugas->id }}" method="post" class="forms-sample"
                        enctype="multipart/form-data">
                        @csrf
                        @method('delete')
                        ANDA YAKIN INGIN MENGHAPUS TOPIK DISKUSI INI? SELURUH FILE YANG DI UPLOAD SISWA AKAN DI HAPUS JUGA
                        DAN HAL INI TIDAK DAPAT DI KEMBALIKAN
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Hapus Tugas</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="{{ asset('vendors/mdi/css/materialdesignicons.min.css') }}">
    <script src="https://cdn.tiny.cloud/1/5gorw5nx4zw5j4viyd3rjucdwe1xqqwublsayv3cd879rzso/tinymce/7/tinymce.min.js"
        referrerpolicy="origin"></script>
@endpush

@push('scripts')
    <script src="https://code.jquery.com/jquery-3.7.1.js"></script>
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.bootstrap4.js"></script>
    <script src="{{ asset('js/swal.min.js') }}"></script>
    <script src="{{ asset('js/file-upload.js') }}"></script>

    <script>
        new DataTable('#example');
    </script>

    <script>
        tinymce.init({
            selector: '#soal',
            plugins: 'advlist autolink autosave code colorpicker emoticons fullscreen hr image imagetools lists link media noneditable nonbreaking pagebreak paste preview print save searchreplace table textcolor textpattern toc visualblocks visualchars wordcount',
            toolbar: 'formatselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | outdent indent | numlist bullist | forecolor backcolor removeformat | link image media | code',
        });
    </script>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            const fileInput = document.querySelector('#file');
            const textareaInput = document.querySelector('#soal');
            const form = document.querySelector('form');

            // Mengatur event listener untuk form submit
            form.addEventListener('submit', function(event) {
                // Cek apakah keduanya kosong
                if (fileInput.files.length === 0 && textareaInput.value.trim() === '') {
                    Swal.fire({
                        title: 'Peringatan',
                        text: "Mohon salah satu jenis soalnya di isi",
                        icon: 'error',
                        confirmButtonText: 'Ok'
                    });
                    event.preventDefault(); // Menghentikan pengiriman form jika keduanya kosong
                }
            });

            // Mengatur event listener untuk input file
            fileInput.addEventListener('change', function() {
                textareaInput.value = ''; // Mengosongkan nilai textarea jika input file diisi
            });

            // Mengatur event listener untuk textarea
            textareaInput.addEventListener('input', function() {
                if (this.value.trim() !== '') {
                    fileInput.value = ''; // Mengosongkan nilai input file jika textarea diisi
                }
            });
        });
    </script>

    <script>
        // Fungsi untuk menampilkan/menyembunyikan form komentar
        document.getElementById('showCommentForm').addEventListener('click', function() {
            var commentFormWrapper = document.getElementById('commentFormWrapper');
            if (commentFormWrapper.style.display === 'none') {
                commentFormWrapper.style.display = 'block';
            } else {
                commentFormWrapper.style.display = 'none';
            }
        });
    </script>

    @if (session('success'))
        <script>
            Swal.fire({
                title: 'Berhasil!',
                text: "{{ session('success') }}",
                icon: 'success',
                confirmButtonText: 'Ok'
            });
        </script>
    @endif

    @if (session('error'))
        <script>
            Swal.fire({
                title: 'Gagal!',
                text: "{{ session('error') }}",
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        </script>
    @endif

    @error('comment')
        <script>
            Swal.fire({
                title: 'Gagal!',
                text: "{{ $message }}",
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        </script>
    @enderror
@endpush
