<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function index()
    {
        $data = Auth::user();

        if($data->role == 'teacher'){
            return view('teacher.profile', ['user' => $data]);
        }else{
            return view('student.profile', ['user' => $data]);
        }
    }

    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'username' => 'required|unique:users,username,' . $request->user()->id,
            'no_handphone' => 'required'
        ]);

        $user_id = Auth::id();
        $data = User::find($user_id);

        $data->name = $request->name;
        if ($data->username !== $request->username) {
            $data->username = $request->username;
        }
        $data->no_handphone = $request->no_handphone;
        if($data->role == 'student'){
            $data->no_hp_orang_tua = $request->no_hp_orang_tua;
        }
        $data->alamat = $request->alamat;
        $data->save();

        if($data->role == 'teacher'){
            return redirect('/teacher/profile')->with('success', 'Profile Berhasil di Perbarui');
        }else{
            return redirect('/student/profile')->with('success', 'Profile Berhasil di Perbarui');
        }
    }

    public function updatepassword(Request $request)
    {
        $request->validate([
            'passwordold' => 'required',
            'passwordnew' => 'required',
            'passwordconfirm' => 'required',
        ]);

        // Simpan password baru ke dalam database
        $user_id = Auth::id();

        $user = User::find($user_id);

        // Periksa apakah password lama cocok dengan yang ada di database
        if (Hash::check($request->passwordold, $user->password)) {
            // Jika cocok, simpan password baru yang dihash
            if ($request->passwordnew == $request->passwordconfirm) {
                $user->password = Hash::make($request->passwordnew);
                $user->save();
                if($user->role == 'teacher'){
                    return redirect('/teacher/profile')->with('success', 'Password Berhasil di Update');
                }else{
                    return redirect('/student/profile')->with('success', 'Password Berhasil di Update');
                }
            }else{
                if($user->role == 'teacher'){
                    return redirect('/teacher/profile')->with('error', 'Password Baru Anda Berbeda dengan Password Konfirmasi');
                }else{
                    return redirect('/student/profile')->with('error', 'Password Baru Anda Berbeda dengan Password Konfirmasi');
                }
            }
        } else {
            // Jika tidak cocok, kembalikan dengan pesan error
            if($user->role == 'teacher'){
                return redirect('/teacher/profile')->with('error', 'Maaf Password yang Anda Masukkan Salah');
            }else{
                return redirect('/student/profile')->with('error', 'Maaf Password yang Anda Masukkan Salah');
            }
        }
    }

    public function login(Request $request)
    {

        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        $credentials = $request->only('username', 'password');

        if (Auth::attempt($credentials)) {
            if(Auth::user()->role == 'teacher'){
                return redirect('/teacher/dashboard');
            }else if(Auth::user()->role == 'student'){
                return redirect('/student/dashboard');
            }else{
                return redirect()->back()->with('error', 'Username atau Password Salah');
            }
        } else {
            return redirect()->back()->with('error', 'Username atau Password Salah');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
