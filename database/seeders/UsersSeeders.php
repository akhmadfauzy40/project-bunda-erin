<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersSeeders extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            [
                'name' => 'Teacher',
                'username' => 'teacher',
                'no_handphone' => '08123456789',
                'password' => Hash::make('teacher'),
                'role' => 'teacher',
                'created_at' => now(),
                'updated_at' => now(),
            ]
        ]);

        DB::table('kelas')->insert([
            [
                'nama_kelas' => 'Kelas A',
                'keterangan' => 'Keterangan kelas A'
            ],
            [
                'nama_kelas' => 'Kelas B',
                'keterangan' => 'Keterangan kelas B'
            ],
            [
                'nama_kelas' => 'Kelas C',
                'keterangan' => 'Keterangan kelas C'
            ],
        ]);

        DB::table('users')->insert([
            [
                'name' => 'Siwa 1',
                'username' => 'siswa1',
                'no_handphone' => '08123456789',
                'no_hp_orang_tua' => '081376544567',
                'password' => Hash::make('student123'),
                'role' => 'student',
                'alamat' => 'Jl. Siswa 1',
                'kelas_id' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => 'Siwa 2',
                'username' => 'siswa2',
                'no_handphone' => '08123456789',
                'no_hp_orang_tua' => '081376544567',
                'password' => Hash::make('student123'),
                'role' => 'student',
                'alamat' => 'Jl. Siswa 2',
                'kelas_id' => 1,
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}
