@extends('layout.welcome')

@section('title')
    Topik Diskusi
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between">
                            <p class="card-title">List Diskusi {{ $data->nama_kelas }}</p>
                        </div>
                        <p class="font-weight-500">Diskusi Terbaru pada {{ $data->nama_kelas }}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="list-group">
                    @foreach ($data->topik()->latest()->get() as $a)
                        <li class="list-group-item">
                            <h5><a href="/student/forum-diskusi/{{ $a->id }}" class="">{{ $a->title }}</a></h5>
                            <ul class="list-unstyled">
                                <li>Tanggal Unggah : {{ $a->created_at->format('Y-m-d H:i:s') }}</li>
                                <li>Deskripsi : {{ $a->deskripsi }}</li>
                            </ul>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="{{ asset('vendors/mdi/css/materialdesignicons.min.css') }}">

    <style>
        /* CSS to change cursor to pointer when hovering over the card */
        .clickable-card:hover {
            cursor: pointer;
        }
    </style>
@endpush

@push('scripts')
    <script src="https://code.jquery.com/jquery-3.7.1.js"></script>
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.bootstrap4.js"></script>
    <script src="{{ asset('js/swal.min.js') }}"></script>
    <script src="{{ asset('js/file-upload.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('.clickable-card').click(function() {
                // Get the URL from data-url attribute of clicked card
                var url = $(this).data('url');

                // Redirect to the specified URL
                window.location.href = url;
            });
        });
    </script>

    <script>
        new DataTable('#example');
    </script>

    @if (session('success'))
        <script>
            Swal.fire({
                title: 'Berhasil!',
                text: "{{ session('success') }}",
                icon: 'success',
                confirmButtonText: 'Ok'
            });
        </script>
    @endif

    @if (session('error'))
        <script>
            Swal.fire({
                title: 'Gagal!',
                text: "{{ session('error') }}",
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        </script>
    @endif

    @error('file')
        <script>
            Swal.fire({
                title: 'Gagal!',
                text: "{{ $message }}",
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        </script>
    @enderror
@endpush
