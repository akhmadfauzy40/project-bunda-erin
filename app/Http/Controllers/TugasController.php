<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use App\Models\PengumpulanTugas;
use App\Models\Tugas;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class TugasController extends Controller
{
    public function index()
    {
        $data = Kelas::all();

        return view('teacher.tugas.index', ['data' => $data]);
    }

    public function list_tugas($id)
    {
        $data = Kelas::find($id);
        return view('teacher.tugas.list-tugas', ['data' => $data]);
    }

    public function store(Request $request)
    {
        // Validasi request
        $request->validate([
            'title' => 'required',
            'deskripsi' => 'required',
            'deadline_tanggal' => 'required',
            'deadline_jam' => 'required',
            'file' => 'file', // Sesuaikan dengan jenis file yang diizinkan
            'kelas_id' => 'required'
        ]);

        // Simpan data topik
        $tugas = new Tugas();
        $tugas->title = $request->title;
        $tugas->deskripsi = $request->deskripsi;
        $tugas->deadline_tanggal = $request->deadline_tanggal;
        $tugas->deadline_jam = $request->deadline_jam;
        if ($request->hasFile('file')) {
            // Simpan file yang baru
            $file = $request->file('file');
            $file_extension = $file->getClientOriginalExtension();
            $file_name = Str::uuid() . '.' . $file_extension;
            $file_path = $file->storeAs('public/tugas_files', $file_name);
            $tugas->path_file = $file_path;
            $tugas->path_file = $file_name;
        }else{
            $tugas->soal = $request->soal;
        }
        $tugas->kelas_id = $request->kelas_id;
        $tugas->save();

        return redirect()->back()->with('success', 'Data Tugas berhasil disimpan');
    }

    public function detail_tugas($id)
    {
        $tugas = Tugas::find($id);

        // $pengumpulan = DB::table('users')
        // ->leftJoin('pengumpulan_tugas', 'users.id', '=', 'pengumpulan_tugas.user_id')
        // ->where('users.kelas_id', $tugas->kelas_id)
        // ->select('users.name', 'pengumpulan_tugas.nilai', 'pengumpulan_tugas.id', 'pengumpulan_tugas.created_at', 'pengumpulan_tugas.user_id')
        // ->get();
        $siswa = User::where('kelas_id', $tugas->kelas_id)->get();
        $pengumpulan = PengumpulanTugas::where('tugas_id', $tugas->id);

        return view('teacher.tugas.detail-tugas', ['tugas' => $tugas, 'user' => $siswa, 'pengumpulan'=>$pengumpulan]);
    }

    public function update(Request $request, $id)
    {
        // Validasi request
        $request->validate([
            'title' => 'required',
            'deskripsi' => 'required',
            'deadline_tanggal' => 'required',
            'deadline_jam' => 'required',
            'file' => 'file', // Sesuaikan dengan jenis file yang diizinkan
        ]);

        // Simpan data topik
        $tugas = Tugas::find($id);
        $tugas->title = $request->title;
        $tugas->deskripsi = $request->deskripsi;
        $tugas->deadline_tanggal = $request->deadline_tanggal;
        $tugas->deadline_jam = $request->deadline_jam;
        if ($request->hasFile('file')) {
            // Menghapus file lama jika ada
            Storage::delete('public/tugas_files/'.$tugas->path_file);
            // Simpan file yang baru
            $file = $request->file('file');
            $file_extension = $file->getClientOriginalExtension();
            $file_name = Str::uuid() . '.' . $file_extension;
            $file_path = $file->storeAs('public/tugas_files', $file_name);
            $tugas->path_file = $file_path;
            $tugas->path_file = $file_name;
            $tugas->soal = null;
        }else{
            Storage::delete('public/tugas_files/'.$tugas->path_file);
            $tugas->soal = $request->soal;
            $tugas->path_file = null;
        }
        $tugas->save();

        return redirect()->back()->with('success', 'Data Tugas berhasil diperbarui');
    }

    public function destroy($id)
    {
        $data = Tugas::find($id);
        $kelas_id = $data->kelas_id;

        foreach ($data->pengumpulan_tugas() as $item){
            Storage::delete('public/tugas_files/'.$item->path_file);
        }

        $data->pengumpulan_tugas()->delete();
        $data->delete();

        return redirect('/teacher/list-tugas/'.$kelas_id)->with('success', 'Data Tugas Berhasil Dihapus');
    }

    public function tugas_siswa($id)
    {
        $pengumpulan = PengumpulanTugas::find($id);

        return view('teacher.tugas.detail-pengumpulan', ['pengumpulan' => $pengumpulan]);
    }

    public function submit_nilai(Request $request, $id)
    {
        $request->validate([
            'nilai' => 'required'
        ]);

        $pengumpulan = PengumpulanTugas::find($id);

        $pengumpulan->nilai = $request->nilai;
        $pengumpulan->catatan_guru = $request->catatan_guru;
        $pengumpulan->save();

        return redirect()->back()->with('success', 'Berhasil Memberi Nilai');
    }
}
