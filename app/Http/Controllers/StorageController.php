<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class StorageController extends Controller
{
    public function createStorageLink()
    {
        try {
            // Menjalankan perintah php artisan storage:link
            Artisan::call('storage:link');

            // Jika berhasil, berikan pesan sukses
            return 'Storage link created successfully.';
        } catch (\Exception $e) {
            // Jika gagal, tangani kesalahan atau berikan pesan error
            return 'Error creating storage link: ' . $e->getMessage();
        }
    }
}
