@extends('layout.welcome')

@section('title')
    Manage Profile
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box">
                        <h4 class="page-title">My Profile</h4>
                    </div>
                </div>
            </div>
            <!-- end page title -->

            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <!-- Preview User Profile Image -->
                                    <div class="mb-3">
                                        <img src="{{ asset('images/person.png') }}" alt="User Profile Image"
                                            class="img-fluid rounded" style="height: 200px;">
                                    </div>

                                    <h4 class="card-title">Profile Information</h4>

                                    <!-- Display User Profile Information -->
                                    <div class="mb-3">
                                        <strong>Name:</strong> {{ $user->name }}
                                    </div>
                                    <div class="mb-3">
                                        <strong>Username:</strong> {{ $user->username }}
                                    </div>
                                    <div class="mb-3">
                                        <strong>No Handphone:</strong> {{ $user->no_handphone }}
                                    </div>
                                    <div class="mb-3">
                                        <strong>No Handphone Orang Tua:</strong> {{ $user->no_hp_orang_tua }}
                                    </div>
                                    <div class="mb-3">
                                        <strong>Alamat:</strong> {{ $user->alamat }}
                                    </div>

                                    <!-- Button to Update Profile -->
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#exampleModal">
                                        Update Profile
                                    </button>
                                </div>

                                <div class="col-md-6">
                                    <h4 class="card-title">Change Password</h4>

                                    <!-- Button to Change Password -->
                                    <button type="button" class="btn btn-warning" data-toggle="modal"
                                        data-target="#changePassword">
                                        Change Password
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Profile</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="/student/profile/edit">
                        @csrf <!-- Tambahkan csrf token untuk keamanan -->
                        @method('put')

                        <div class="mb-3">
                            <label for="name" class="form-label">Nama</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name"
                                name="name" value="{{ $user->name }}" aria-describedby="textHelp">
                            @error('name')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="mb-3">
                            <label for="username" class="form-label">Username</label>
                            <input type="text" class="form-control @error('username') is-invalid @enderror"
                                id="username" name="username" value="{{ $user->username }}" aria-describedby="emailHelp">
                            @error('username')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="mb-3">
                            <label for="no_handphone" class="form-label">No Handphone</label>
                            <input type="text" class="form-control @error('no_handphone') is-invalid @enderror"
                                id="no_handphone" name="no_handphone" value="{{ $user->no_handphone }}">
                            @error('no_handphone')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="mb-3">
                            <label for="no_hp_orang_tua" class="form-label">No Handphone Orang Tua</label>
                            <input type="text" class="form-control @error('no_hp_orang_tua') is-invalid @enderror"
                                id="no_hp_orang_tua" name="no_hp_orang_tua" value="{{ $user->no_hp_orang_tua }}">
                            @error('no_hp_orang_tua')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <textarea class="form-control @error('alamat') is-invalid @enderror" id="alamat" rows="4" name="alamat" placeholder="Alamat Siswa">{{ $user->alamat }}</textarea>
                            @error('alamat')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="changePassword" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Ubah Password</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/student/profile/editpassword" method="post">
                        @csrf
                        @method('put')
                        <div class="mb-4">
                            <label for="passwordold" class="form-label">Password Sekarang</label>
                            <input type="password" class="form-control" id="passwordold" name="passwordold">
                        </div>
                        <div class="mb-4">
                            <label for="passwordnew" class="form-label">Password Baru</label>
                            <input type="password" class="form-control" id="passwordnew" name="passwordnew">
                        </div>
                        <div class="mb-4">
                            <label for="passwordconfirm" class="form-label">Konfirmasi Password</label>
                            <input type="password" class="form-control" id="passwordconfirm"
                                name="passwordconfirm">
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
@endpush

@push('scripts')
    <script src="{{ asset('js/swal.min.js') }}"></script>
    @if (session('success'))
        <script>
            Swal.fire({
                title: 'Berhasil!',
                text: "{{ session('success') }}",
                icon: 'success',
                confirmButtonText: 'Ok'
            });
        </script>
    @endif

    @if (session('error'))
        <script>
            Swal.fire({
                title: 'Gagal!',
                text: "{{ session('error') }}",
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        </script>
    @endif

    @error('username')
        <script>
            Swal.fire({
                title: 'Gagal!',
                text: "{{ $message }}",
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        </script>
    @enderror
@endpush
