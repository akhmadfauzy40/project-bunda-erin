<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use App\Models\PengumpulanTugas;
use App\Models\Topik;
use App\Models\Tugas;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function student_dashboard()
    {
        $total_diskusi = Topik::where('kelas_id', Auth::user()->kelas_id)->count();
        $total_tugas = Tugas::where('kelas_id', Auth::user()->kelas_id)->count();
        $total_tugas_dikerjakan = PengumpulanTugas::where('user_id', Auth::id())->count();
        $total_tugas_belum_dikerjakan = $total_tugas - $total_tugas_dikerjakan;

        return view('student.dashboard', ['total_diskusi'=>$total_diskusi, 'total_tugas'=>$total_tugas, 'total_tugas_dikerjakan'=>$total_tugas_dikerjakan, 'total_tugas_belum_dikerjakan'=>$total_tugas_belum_dikerjakan]);
    }

    public function teacher_dashboard()
    {
        $total_kelas = Kelas::count();
        $total_siswa = User::where('role', 'student')->count();
        $total_diskusi = Topik::count();
        $total_tugas = Tugas::count();

        return view('teacher.dashboard', ['total_kelas'=>$total_kelas, 'total_siswa'=>$total_siswa, 'total_diskusi'=>$total_diskusi, 'total_tugas'=>$total_tugas]);
    }
}
