@extends('layout.welcome')

@section('title')
    Topik Diskusi
@endsection

@section('content')
    @php
        use Carbon\Carbon;
    @endphp
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">{{ $data->title }}</h5>
                        <p class="font-weight-500">{{ $data->deskripsi }}</p>
                        <p class="font-weight-500"><a href="{{ asset('storage/topik_files/' . $data->path_file) }}"
                                target="_blank">Klik Disini</a> untuk mendownload
                            materi diskusi</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between mb-3">
                            <h5 class="card-title">Forum Diskusi</h5>
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                data-target="#exampleModal">
                                Tambah Komentar
                            </button>
                        </div>
                        <ul class="list-group" id="commentList">
                            @foreach ($data->diskusi as $item)
                                <li class="list-group-item">
                                    <p class="font-weight-500">
                                        @if ($item->user_id == Auth::id())
                                            Anda
                                        @else
                                            {{ $item->user->name }}
                                        @endif
                                        <span class="text-muted ml-1">•
                                            {{ Carbon::parse($item->created_at)->diffForHumans() }}</span>
                                    </p>
                                    <p>
                                        {{ $item->comment }}
                                    </p>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambahkan Komentar Anda</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/student/send-comment" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="comment">Komentar Anda</label>
                            <textarea class="form-control @error('comment') is-invalid @enderror" id="comment" rows="4" name="comment"
                                placeholder="Komentar Anda"></textarea>
                            @error('comment')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                            <input type="hidden" name="topik_id" value="{{ $data->id }}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Kirim</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="{{ asset('vendors/mdi/css/materialdesignicons.min.css') }}">
@endpush

@push('scripts')
    <script src="https://code.jquery.com/jquery-3.7.1.js"></script>
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.bootstrap4.js"></script>
    <script src="{{ asset('js/swal.min.js') }}"></script>
    <script src="{{ asset('js/file-upload.js') }}"></script>

    <script>
        new DataTable('#example');
    </script>

    <script>
        // Fungsi untuk menampilkan/menyembunyikan form komentar
        document.getElementById('showCommentForm').addEventListener('click', function() {
            var commentFormWrapper = document.getElementById('commentFormWrapper');
            if (commentFormWrapper.style.display === 'none') {
                commentFormWrapper.style.display = 'block';
            } else {
                commentFormWrapper.style.display = 'none';
            }
        });
    </script>

    @if (session('success'))
        <script>
            Swal.fire({
                title: 'Berhasil!',
                text: "{{ session('success') }}",
                icon: 'success',
                confirmButtonText: 'Ok'
            });
        </script>
    @endif

    @if (session('error'))
        <script>
            Swal.fire({
                title: 'Gagal!',
                text: "{{ session('error') }}",
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        </script>
    @endif

    @error('comment')
        <script>
            Swal.fire({
                title: 'Gagal!',
                text: "{{ $message }}",
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        </script>
    @enderror
@endpush
