<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use App\Models\PengumpulanTugas;
use App\Models\Tugas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class TugasSiswaController extends Controller
{
    public function index()
    {
        $data = Kelas::find(Auth::user()->kelas_id);

        return view('student.tugas.index', ['data' => $data]);
    }

    public function detail_tugas($id)
    {
        $tugas = Tugas::find($id);

        // Mendapatkan ID pengguna yang sedang terautentikasi
        $user_id = auth()->user()->id;

        // Mengambil data pengumpulan tugas berdasarkan tugas_id dan user_id
        $pengumpulan = PengumpulanTugas::where('tugas_id', $id)
            ->where('user_id', $user_id)
            ->first();

        return view('student.tugas.pengumpulan', ['tugas' => $tugas, 'pengumpulan' => $pengumpulan]);
    }

    public function store(Request $request){

        $pengumpulan = new PengumpulanTugas();

        if ($request->hasFile('file')) {
            // Simpan file yang baru
            $file = $request->file('file');
            $file_extension = $file->getClientOriginalExtension();
            $file_name = Str::uuid() . '.' . $file_extension;
            $file_path = $file->storeAs('public/tugas_files', $file_name);
            $pengumpulan->path_file = $file_path;
            $pengumpulan->path_file = $file_name;
        }else{
            $pengumpulan->jawab_essay = $request->jawab_essay;
        }

        $pengumpulan->tugas_id = $request->tugas_id;
        $pengumpulan->user_id = Auth::id();
        $pengumpulan->save();
        return redirect()->back()->with('success', 'Anda Berhasil Submit Tugas');
    }

    public function update(Request $request, $id){

        $pengumpulan = PengumpulanTugas::find($id);

        if ($request->hasFile('file')) {
            // Menghapus file lama jika ada
            Storage::delete('public/tugas_files/'.$pengumpulan->path_file);
            // Simpan file yang baru
            $file = $request->file('file');
            $file_extension = $file->getClientOriginalExtension();
            $file_name = Str::uuid() . '.' . $file_extension;
            $file_path = $file->storeAs('public/tugas_files', $file_name);
            $pengumpulan->path_file = $file_path;
            $pengumpulan->path_file = $file_name;
            $pengumpulan->jawab_essay = null;
        }else{
            // Menghapus file lama jika ada
            Storage::delete('public/tugas_files/'.$pengumpulan->path_file);
            $pengumpulan->path_file = null;
            $pengumpulan->jawab_essay = $request->jawab_essay;
        }
        $pengumpulan->save();
        return redirect()->back()->with('success', 'Anda Berhasil Memperbarui Tugas');
    }
}
