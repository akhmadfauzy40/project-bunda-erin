<?php

namespace App\Http\Controllers;

use App\Models\Diskusi;
use App\Models\Topik;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DiskusiController extends Controller
{
    public function index($id)
    {
        $data = Topik::find($id);

        return view('teacher.topik-diskusi.forum-diskusi', ['data' => $data]);
    }

    public function send_comment(Request $request)
    {
        $request->validate([
            'comment' => 'required',
        ]);

        $data = new Diskusi();
        $data->comment = $request->comment;
        $data->topik_id = $request->topik_id;
        $data->user_id = Auth::id();
        $data->save();

        return redirect()->back()->with('success', 'Tanggapan Anda Sudah di Kirimkan');
    }
}
