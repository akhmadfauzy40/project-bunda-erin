<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SiswaController extends Controller
{
    public function index()
    {
        $data = User::where('role', 'student')->get();
        $kelas = Kelas::all();

        return view('teacher.siswa', ['data' => $data, 'kelas' => $kelas]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'username' => 'required|unique:users,username',
            'kelas_id' => 'required',
        ]);

        $data = new User();
        $data->name = $request->name;
        $data->username = $request->username;
        $data->no_handphone = $request->no_handphone;
        $data->no_hp_orang_tua = $request->no_hp_orang_tua;
        $data->password = Hash::make('student123');
        $data->role = 'student';
        $data->alamat = $request->alamat;
        $data->kelas_id = $request->kelas_id;
        $data->save();

        return redirect('/teacher/manage-siswa')->with('success', 'Berhasil Menyimpan Data Siswa');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'username' => 'required|unique:users,username,' . $id,
            'kelas_id' => 'required',
        ]);

        $data = User::find($id);
        $data->name = $request->name;
        $data->username = $request->username;
        $data->no_handphone = $request->no_handphone;
        $data->no_hp_orang_tua = $request->no_hp_orang_tua;
        $data->alamat = $request->alamat;
        $data->kelas_id = $request->kelas_id;
        $data->save();

        return redirect('/teacher/manage-siswa')->with('success', 'Berhasil Memperbarui Data Siswa');
    }

    public function destroy($id)
    {
        $data = User::find($id);

        $relationsToCheck = ['diskusi', 'pengumpulan_tugas'];

        foreach ($relationsToCheck as $relation) {
            // Cek apakah user memiliki relasi
            $hasRelation = $data->{$relation}()->exists();

            if ($hasRelation) {
                return redirect()->back()->with('error', 'Gagal menghapus data: User ini memiliki ' . $relation . '.');
            }
        }

        // Hapus data jika tidak memiliki relasi
        if ($data->delete()) {
            return redirect()->back()->with('success', 'Berhasil Menghapus Data Siswa');
        } else {
            return redirect()->back()->with('error', 'Gagal menghapus data.');
        }
    }
}
