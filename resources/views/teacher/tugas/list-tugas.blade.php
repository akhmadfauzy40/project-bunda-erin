@extends('layout.welcome')

@section('title')
    Manage Tugas
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="row mb-3">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">List Tugas {{ $data->nama_kelas }}</h5>
                        <p class="card-text">Silakan Kelola Tugas di Kelas {{ $data->nama_kelas }}</p>
                        <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#exampleModal">
                            Tambah Tugas
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <ul class="list-group">
                    @foreach ($data->tugas()->latest()->get() as $a)
                        <li class="list-group-item">
                            <h5><a href="/teacher/detail-tugas/{{ $a->id }}" class="">{{ $a->title }}</a>
                            </h5>
                            <ul class="list-unstyled">
                                <li>Tanggal Unggah : {{ $a->created_at->format('Y-m-d H:i:s') }}</li>
                                <li>Deadline Tugas : {{ $a->deadline_tanggal }} {{ $a->deadline_jam }}</li>
                                <li>Deskripsi: {{ $a->deskripsi }}</li>
                            </ul>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>


    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Tugas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/teacher/add-tugas" method="post" class="forms-sample" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="title">Judul Tugas</label>
                            <input type="text" class="form-control @error('title') is-invalid @enderror" id="title"
                                placeholder="Masukkan Judul Topik Materi" name="title">
                            @error('title')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="deskripsi">Deskripsi Singkat Tugas</label>
                            <textarea class="form-control @error('deskripsi') is-invalid @enderror" id="deskripsi" rows="4" name="deskripsi"
                                placeholder="Masukkan Deskripsi Topik Materi"></textarea>
                            @error('deskripsi')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="deadline_tanggal">Deadline Tugas (Hari & Tanggal)</label>
                            <input type="date" name="deadline_tanggal" id="deadline_tanggal"
                                class="form-control @error('deadline_tanggal') is-invalid @enderror">
                            @error('deadline_tanggal')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="deadline_jam">Deadline Tugas (Jam)</label>
                            <input type="time" name="deadline_jam" id="deadline_jam"
                                class="form-control @error('deadline_jam') is-invalid @enderror">
                            @error('deadline_jam')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active" id="home-tab" data-toggle="tab" data-target="#jawab_file"
                                    type="button" role="tab" aria-controls="home" aria-selected="true">Soal
                                    Bentuk File</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="profile-tab" data-toggle="tab" data-target="#jawab_manual"
                                    type="button" role="tab" aria-controls="profile" aria-selected="false">Ketik Soal
                                    Manual</button>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="jawab_file" role="tabpanel"
                                aria-labelledby="home-tab">
                                <div class="form-group">
                                    <label>Upload File Soal</label>
                                    <input type="file" name="file" id="file"
                                        class="file-upload-default @error('file') is-invalid @enderror">
                                    <div class="input-group col-xs-12">
                                        <input type="text" class="form-control file-upload-info" disabled
                                            placeholder="Upload File Tugas">
                                        <span class="input-group-append">
                                            <button class="file-upload-browse btn btn-primary"
                                                type="button">Upload</button>
                                        </span>
                                    </div>
                                    @error('file')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <div class="tab-pane fade" id="jawab_manual" role="tabpanel" aria-labelledby="profile-tab">
                                <div class="form-group">
                                    <label for="soal">Silahkan jawab pada kolom dibawah ini</label>
                                    <textarea class="form-control @error('soal') is-invalid @enderror" id="soal" rows="4" name="soal"></textarea>
                                    @error('soal')
                                        <div class="invalid-feedback">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="kelas_id" value="{{ $data->id }}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan Data Tugas</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="{{ asset('vendors/mdi/css/materialdesignicons.min.css') }}">
    <script src="https://cdn.tiny.cloud/1/5gorw5nx4zw5j4viyd3rjucdwe1xqqwublsayv3cd879rzso/tinymce/7/tinymce.min.js"
        referrerpolicy="origin"></script>
@endpush

@push('scripts')
    <script src="https://code.jquery.com/jquery-3.7.1.js"></script>
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.bootstrap4.js"></script>
    <script src="{{ asset('js/swal.min.js') }}"></script>
    <script src="{{ asset('js/file-upload.js') }}"></script>

    <script>
        tinymce.init({
            selector: '#soal',
            plugins: 'advlist autolink autosave code colorpicker emoticons fullscreen hr image imagetools lists link media noneditable nonbreaking pagebreak paste preview print save searchreplace table textcolor textpattern toc visualblocks visualchars wordcount',
            toolbar: 'formatselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | outdent indent | numlist bullist | forecolor backcolor removeformat | link image media | code',
        });
    </script>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            const fileInput = document.querySelector('#file');
            const textareaInput = document.querySelector('#soal');
            const form = document.querySelector('form');

            // Mengatur event listener untuk form submit
            form.addEventListener('submit', function(event) {
                // Cek apakah keduanya kosong
                if (fileInput.files.length === 0 && textareaInput.value.trim() === '') {
                    Swal.fire({
                        title: 'Peringatan',
                        text: "Mohon salah satu jenis soalnya di isi",
                        icon: 'error',
                        confirmButtonText: 'Ok'
                    });
                    event.preventDefault(); // Menghentikan pengiriman form jika keduanya kosong
                }
            });

            // Mengatur event listener untuk input file
            fileInput.addEventListener('change', function() {
                textareaInput.value = ''; // Mengosongkan nilai textarea jika input file diisi
            });

            // Mengatur event listener untuk textarea
            textareaInput.addEventListener('input', function() {
                if (this.value.trim() !== '') {
                    fileInput.value = ''; // Mengosongkan nilai input file jika textarea diisi
                }
            });
        });
    </script>


    <script>
        $(document).ready(function() {
            $('.clickable-card').click(function() {
                // Get the URL from data-url attribute of clicked card
                var url = $(this).data('url');

                // Redirect to the specified URL
                window.location.href = url;
            });
        });
    </script>

    <script>
        new DataTable('#example');
    </script>

    @if (session('success'))
        <script>
            Swal.fire({
                title: 'Berhasil!',
                text: "{{ session('success') }}",
                icon: 'success',
                confirmButtonText: 'Ok'
            });
        </script>
    @endif

    @if (session('error'))
        <script>
            Swal.fire({
                title: 'Gagal!',
                text: "{{ session('error') }}",
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        </script>
    @endif

    @error('file')
        <script>
            Swal.fire({
                title: 'Gagal!',
                text: "{{ $message }}",
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        </script>
    @enderror
@endpush
