<?php

namespace App\Http\Controllers;

use App\Models\Diskusi;
use App\Models\Kelas;
use App\Models\Topik;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class TopikController extends Controller
{
    public function index()
    {
        $data = Kelas::all();
        return view('teacher.topik-diskusi.index', ['data' => $data]);
    }

    public function list_diskusi($id)
    {
        $data = Kelas::find($id);
        return view('teacher.topik-diskusi.list-diskusi', ['data' => $data]);
    }

    public function store(Request $request)
    {
        // Validasi request
        $request->validate([
            'title' => 'required',
            'deskripsi' => 'required',
            'file' => 'required|file', // Sesuaikan dengan jenis file yang diizinkan
            'kelas_id' => 'required'
        ]);

        // Simpan file dengan nama UUID
        $file = $request->file('file');
        $file_extension = $file->getClientOriginalExtension(); // Dapatkan ekstensi file
        $file_name = Str::uuid() . '.' . $file_extension; // Buat nama file dengan UUID
        $file_path = $file->storeAs('public/topik_files', $file_name); // Simpan file dengan nama UUID

        // Simpan data topik
        $topik = new Topik();
        $topik->title = $request->title;
        $topik->deskripsi = $request->deskripsi;
        $topik->path_file = $file_path;
        $file_path = $file_name;
        $topik->path_file = $file_path;
        $topik->kelas_id = $request->kelas_id;
        $topik->save();

        return redirect()->back()->with('success', 'Data topik berhasil disimpan');
    }

    public function update(Request $request, $id)
    {
        // Validasi request
        $request->validate([
            'title' => 'required',
            'deskripsi' => 'required',
            'file' => 'file', // Sesuaikan dengan jenis file yang diizinkan
        ]);

        // Simpan data topik
        $topik = Topik::find($id);
        $topik->title = $request->title;
        $topik->deskripsi = $request->deskripsi;

        if ($request->hasFile('file')) {
            // Menghapus file lama jika ada
            Storage::delete('public/topik_files/'.$topik->path_file);

            // Simpan file yang baru
            $file = $request->file('file');
            $file_extension = $file->getClientOriginalExtension();
            $file_name = Str::uuid() . '.' . $file_extension;
            $file_path = $file->storeAs('public/topik_files', $file_name);
            $topik->path_file = $file_path;
            $topik->path_file = $file_name;
        }

        $topik->save();

        return redirect()->back()->with('success', 'Data topik berhasil diupdate');
    }

    public function destroy($id)
    {
        $data = Topik::find($id);
        $kelas_id = $data->kelas_id;
        $data->diskusi()->delete();
        Storage::delete('public/topik_files/'.$data->path_file);
        $data->delete();

        return redirect('/teacher/list-diskusi/'.$kelas_id)->with('success', 'Topik Pembahasan Berhasil Dihapus');
    }
}
