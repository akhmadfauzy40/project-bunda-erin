<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title')</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ asset('vendors/feather/feather.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/ti-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/css/vendor.bundle.base.css') }}">
    <!-- endinject -->
    <!-- Plugin css for this page -->
    <link rel="stylesheet" href="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/ti-icons/css/themify-icons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('js/select.dataTables.min.css') }}">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('css/vertical-layout-light/style.css') }}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{ asset('images/logo1.png') }}" />
    @stack('styles')
</head>

<body>
    <div class="container-scroller">
        <!-- partial:partials/_navbar.html -->
        <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
            <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
                @if (Auth::user()->role == 'teacher')
                    <a class="navbar-brand brand-logo mr-5" href="/teacher/dashboard"><img
                            src="{{ asset('images/logo2.png') }}" class="mr-2" alt="logo"
                            style="height: 70px;" /></a>
                    <a class="navbar-brand brand-logo-mini" href="/teacher/dashboard"><img
                            src="{{ asset('images/logo1.png') }}" alt="logo" style="height: 50px;" /></a>
                @else
                    <a class="navbar-brand brand-logo mr-5" href="/student/dashboard"><img
                            src="{{ asset('images/logo2.png') }}" class="mr-2" alt="logo"
                            style="height: 70px;" /></a>
                    <a class="navbar-brand brand-logo-mini" href="/student/dashboard"><img
                            src="{{ asset('images/logo1.png') }}" alt="logo" style="height: 50px;" /></a>
                @endif

            </div>
            <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
                <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
                    <span class="icon-menu"></span>
                </button>
                <ul class="navbar-nav navbar-nav-right">
                    <li class="nav-item nav-profile dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                            <img src="{{ asset('images/person.png') }}" alt="profile" />
                        </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown"
                            aria-labelledby="profileDropdown">
                            <a href="/logout" class="dropdown-item">
                                <i class="ti-power-off text-primary"></i>
                                Logout
                            </a>
                        </div>
                    </li>
                </ul>
                <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
                    data-toggle="offcanvas">
                    <span class="icon-menu"></span>
                </button>
            </div>
        </nav>
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
            <!-- partial:partials/_settings-panel.html -->
            <!-- partial -->
            <!-- partial:partials/_sidebar.html -->
            <nav class="sidebar sidebar-offcanvas" id="sidebar">
                <ul class="nav">
                    @if (Auth::user()->role == 'teacher')
                        <li class="nav-item">
                            <a class="nav-link" href="/teacher/dashboard">
                                <i class="ti-bar-chart menu-icon"></i>
                                <!-- Menggunakan kelas themify-icons untuk ikon grafik -->
                                <span class="menu-title">Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false"
                                aria-controls="ui-basic">
                                <i class="ti-layout menu-icon"></i>
                                <!-- Menggunakan kelas themify-icons untuk ikon Data Manager -->
                                <span class="menu-title">Data Manager</span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="collapse" id="ui-basic">
                                <ul class="nav flex-column sub-menu">
                                    <li class="nav-item"> <a class="nav-link" href="/teacher/manage-kelas">Manage
                                            Kelas</a></li>
                                    <li class="nav-item"> <a class="nav-link" href="/teacher/manage-siswa">Manage
                                            Siswa</a></li>
                                    <li class="nav-item"> <a class="nav-link" href="/teacher/topik-diskusi">Manage
                                            Materi</a></li>
                                    <li class="nav-item"> <a class="nav-link" href="/teacher/tugas">Manage Tugas</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/teacher/profile">
                                <i class="ti-user menu-icon"></i>
                                <!-- Menggunakan kelas themify-icons untuk ikon Profile -->
                                <span class="menu-title">Profile</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/logout">
                                <i class="ti-power-off menu-icon"></i>
                                <!-- Menggunakan kelas themify-icons untuk ikon Logout -->
                                <span class="menu-title">Logout</span>
                            </a>
                        </li>
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="/student/dashboard">
                                <i class="ti-bar-chart menu-icon"></i>
                                <!-- Menggunakan kelas themify-icons untuk ikon grafik -->
                                <span class="menu-title">Dashboard</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/student/forum-diskusi">
                                <i class="ti-comment-alt menu-icon"></i>
                                <!-- Menggunakan kelas themify-icons untuk ikon Profile -->
                                <span class="menu-title">Forum Diskusi</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/student/tugas">
                                <i class="ti-pencil-alt menu-icon"></i>
                                <!-- Menggunakan kelas themify-icons untuk ikon Profile -->
                                <span class="menu-title">Tugas</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/student/profile">
                                <i class="ti-user menu-icon"></i>
                                <!-- Menggunakan kelas themify-icons untuk ikon Profile -->
                                <span class="menu-title">Profile</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/logout">
                                <i class="ti-power-off menu-icon"></i>
                                <!-- Menggunakan kelas themify-icons untuk ikon Logout -->
                                <span class="menu-title">Logout</span>
                            </a>
                        </li>
                    @endif
                </ul>
            </nav>
            <!-- partial -->
            <div class="main-panel">
                @yield('content')
                <!-- content-wrapper ends -->
                <!-- partial:partials/_footer.html -->
                <footer class="footer">
                    <div class="d-sm-flex justify-content-center justify-content-sm-between">
                        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2021.
                            Premium <a href="https://www.bootstrapdash.com/" target="_blank">Bootstrap admin
                                template</a> from BootstrapDash. All rights reserved.</span>
                        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made
                            with <i class="ti-heart text-danger ml-1"></i></span>
                    </div>
                    <div class="d-sm-flex justify-content-center justify-content-sm-between">
                        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Distributed by <a
                                href="https://www.themewagon.com/" target="_blank">Themewagon</a></span>
                    </div>
                </footer>
                <!-- partial -->
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->

    <!-- plugins:js -->
    <script src="{{ asset('vendors/js/vendor.bundle.base.js') }}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{ asset('vendors/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('js/dataTables.select.min.js') }}"></script>

    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{ asset('js/off-canvas.js') }}"></script>
    <script src="{{ asset('js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('js/template.js') }}"></script>
    <script src="{{ asset('js/settings.js') }}"></script>
    <script src="{{ asset('js/todolist.js') }}"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <script src="{{ asset('js/dashboard.js') }}"></script>
    <script src="{{ asset('js/Chart.roundedBarCharts.js') }}"></script>
    <!-- End custom js for this page-->
    @stack('scripts')
</body>

</html>
