@extends('layout.welcome')

@section('title')
    Manage Topik Diskusi
@endsection

@section('content')
    @php
        use Carbon\Carbon;
    @endphp
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center">
                            <h5 class="card-title">{{ $data->title }}</h5>
                            <div class="dropdown">
                                <button type="button" class="btn btn-outline-secondary btn-rounded btn-icon" type="button"
                                    id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">
                                    <i class="mdi mdi-dots-horizontal"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    <button type="button" class="dropdown-item" data-toggle="modal"
                                        data-target="#editTopik">
                                        Edit Topik Pembahasan
                                    </button>
                                    <button type="button" class="dropdown-item" data-toggle="modal"
                                        data-target="#hapusTopik">
                                        Hapus Topik Pembahasan
                                    </button>
                                </div>
                            </div>
                        </div>
                        <p class="font-weight-500">{{ $data->deskripsi }}</p>
                        <p class="font-weight-500"><a href="{{ asset('storage/topik_files/' . $data->path_file) }}"
                                target="_blank">Klik Disini</a> untuk mendownload
                            materi diskusi</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between mb-3">
                            <h5 class="card-title">Forum Diskusi</h5>
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                data-target="#exampleModal">
                                Tambah Komentar
                            </button>
                        </div>
                        <ul class="list-group" id="commentList">
                            @foreach ($data->diskusi as $item)
                                <li class="list-group-item">
                                    <p class="font-weight-500">
                                        @if ($item->user_id == Auth::id())
                                            Anda
                                        @else
                                            {{ $item->user->name }}
                                        @endif
                                        <span class="text-muted ml-1">• {{ Carbon::parse($item->created_at)->diffForHumans() }}</span>
                                    </p>
                                    <p>
                                        {{ $item->comment }}
                                    </p>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambahkan Komentar Anda</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/teacher/send-comment" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="comment">Komentar Anda</label>
                            <textarea class="form-control @error('comment') is-invalid @enderror" id="comment" rows="4" name="comment"
                                placeholder="Komentar Anda"></textarea>
                            @error('comment')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                            <input type="hidden" name="topik_id" value="{{ $data->id }}">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Kirim</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editTopik" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update Topik Diskusi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/teacher/update-topik/{{ $data->id }}" method="post" class="forms-sample" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label for="title">Judul Topik Materi</label>
                            <input type="text" class="form-control @error('title') is-invalid @enderror" id="title"
                                placeholder="Masukkan Judul Topik Materi" name="title" value="{{ $data->title }}">
                            @error('title')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="deskripsi">Deskripsi Singkat Materi</label>
                            <textarea class="form-control @error('deskripsi') is-invalid @enderror" id="deskripsi" rows="4"
                                name="deskripsi" placeholder="Masukkan Deskripsi Topik Materi">{{ $data->deskripsi }}</textarea>
                            @error('deskripsi')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>File upload</label>
                            <input type="file" name="file"
                                class="file-upload-default @error('file') is-invalid @enderror">
                            <div class="input-group col-xs-12">
                                <input type="text" class="form-control file-upload-info" disabled
                                    placeholder="Upload Image">
                                <span class="input-group-append">
                                    <button class="file-upload-browse btn btn-primary" type="button">Upload</button>
                                </span>
                            </div>
                            @error('file')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                            <small class="form-text text-muted">* Kosongkan jika tidak ingin mengubah file materi</small>
                        </div>


                        <input type="hidden" name="kelas_id" value="{{ $data->id }}">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Update Topik Diskusi</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="hapusTopik" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Hapus Topik Diskusi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/teacher/delete-topik/{{ $data->id }}" method="post" class="forms-sample" enctype="multipart/form-data">
                        @csrf
                        @method('delete')
                        ANDA YAKIN INGIN MENGHAPUS TOPIK DISKUSI INI? HAL INI TIDAK DAPAT DI KEMBALIKAN
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Hapus Topik Diskusi</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="{{ asset('vendors/mdi/css/materialdesignicons.min.css') }}">
@endpush

@push('scripts')
    <script src="https://code.jquery.com/jquery-3.7.1.js"></script>
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.bootstrap4.js"></script>
    <script src="{{ asset('js/swal.min.js') }}"></script>
    <script src="{{ asset('js/file-upload.js') }}"></script>

    <script>
        new DataTable('#example');
    </script>

    <script>
        // Fungsi untuk menampilkan/menyembunyikan form komentar
        document.getElementById('showCommentForm').addEventListener('click', function() {
            var commentFormWrapper = document.getElementById('commentFormWrapper');
            if (commentFormWrapper.style.display === 'none') {
                commentFormWrapper.style.display = 'block';
            } else {
                commentFormWrapper.style.display = 'none';
            }
        });
    </script>

    @if (session('success'))
        <script>
            Swal.fire({
                title: 'Berhasil!',
                text: "{{ session('success') }}",
                icon: 'success',
                confirmButtonText: 'Ok'
            });
        </script>
    @endif

    @if (session('error'))
        <script>
            Swal.fire({
                title: 'Gagal!',
                text: "{{ session('error') }}",
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        </script>
    @endif

    @error('comment')
        <script>
            Swal.fire({
                title: 'Gagal!',
                text: "{{ $message }}",
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        </script>
    @enderror
@endpush
