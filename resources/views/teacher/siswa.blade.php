@extends('layout.welcome')

@section('title')
    Manage Siswa
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <p class="card-title">Data Siswa</p>
                    </div>
                    <p class="font-weight-500">Silahkan Manage Data Siswa Anda</p>
                    <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#exampleModal">
                        Tambah Data Siswa
                    </button>
                    <div class="table-responsive">
                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Siswa</th>
                                    <th>Username Siswa</th>
                                    <th>Kelas</th>
                                    <th>No Handphone Siswa</th>
                                    <th>No Handphone Orang Tua</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $no => $item)
                                    <tr>
                                        <td>{{ $no + 1 }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->username }}</td>
                                        <td>{{ $item->kelas->nama_kelas }}</td>
                                        <td>
                                            @if ($item->no_handphone != '')
                                                {{ $item->no_handphone }}
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>
                                            @if ($item->no_hp_orang_tua != '')
                                                {{ $item->no_hp_orang_tua }}
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            <button type="button" class="btn btn-danger" data-toggle="modal"
                                                data-target="#deleteData{{ $no }}">
                                                <i class="mdi mdi-delete"></i>
                                            </button>
                                            <button type="button" class="btn btn-warning" data-toggle="modal"
                                                data-target="#editData{{ $no }}">
                                                <i class="mdi mdi-lead-pencil"></i>
                                            </button>
                                        </td>
                                    </tr>

                                    <div class="modal fade" id="editData{{ $no }}" tabindex="-1"
                                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Update Data Siswa</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="/teacher/update-siswa/{{ $item->id }}" method="post"
                                                        class="forms-sample">
                                                        @csrf
                                                        @method('put')
                                                        <div class="form-group">
                                                            <label for="nama">Nama Siswa</label>
                                                            <input type="text" class="form-control" id="nama"
                                                                placeholder="Nama Siswa" name="name"
                                                                value="{{ $item->name }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="username">Username Siswa</label>
                                                            <input type="text" class="form-control" id="username"
                                                                placeholder="Username Siswa" name="username"
                                                                value="{{ $item->username }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="kelas">Kelas Siswa</label>
                                                            <select class="form-control" id="kelas" name="kelas_id">
                                                                <option disabled>- Silahkan Pilih Kelas -</option>
                                                                @foreach ($kelas as $kls)
                                                                    <option value="{{ $kls->id }}"
                                                                        @if ($kls->id == $item->kelas_id) selected @endif>
                                                                        {{ $kls->nama_kelas }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="no_handphone">Nomor Handphone Siswa</label>
                                                            <input type="text" class="form-control" id="no_handphone"
                                                                placeholder="No Handphone Siswa" name="no_handphone"
                                                                value="{{ $item->no_handphone }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="no_hp_orang_tua">Nomor Handphone Orang Tua</label>
                                                            <input type="text" class="form-control" id="no_hp_orang_tua"
                                                                placeholder="No Handphone Orang Tua" name="no_hp_orang_tua"
                                                                value="{{ $item->no_hp_orang_tua }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="alamat">Alamat</label>
                                                            <textarea class="form-control" id="alamat" rows="4" name="alamat" placeholder="Alamat Siswa">{{ $item->alamat }}</textarea>
                                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-primary">Simpan Data
                                                        Siswa</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal fade" id="deleteData{{ $no }}" tabindex="-1"
                                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Hapus Data Kelas</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="/teacher/delete-siswa/{{ $item->id }}"
                                                        method="post" class="forms-sample">
                                                        @csrf
                                                        @method('delete')
                                                        <p>ANDA YAKIN INGIN MENGHAPUS DATA INI?? DATA YANG DI HAPUS TIDAK
                                                            AKAN
                                                            DAPAT KEMBALI</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Close</button>
                                                    <button type="submit" class="btn btn-danger">Hapus Data
                                                        Siswa</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Siswa</th>
                                    <th>Username Siswa</th>
                                    <th>Kelas</th>
                                    <th>No Handphone Siswa</th>
                                    <th>No Handphone Orang Tua</th>
                                    <th>Aksi</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Siswa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/teacher/add-siswa" method="post" class="forms-sample">
                        @csrf
                        <div class="form-group">
                            <label for="nama">Nama Siswa</label>
                            <input type="text" class="form-control" id="nama" placeholder="Nama Siswa"
                                name="name">
                        </div>
                        <div class="form-group">
                            <label for="username">Username Siswa</label>
                            <input type="text" class="form-control" id="username" placeholder="Username Siswa"
                                name="username">
                        </div>
                        <div class="form-group">
                            <label for="kelas">Kelas Siswa</label>
                            <select class="form-control" id="kelas" name="kelas_id">
                                <option selected disabled>- Silahkan Pilih Kelas -</option>
                                @foreach ($kelas as $kls)
                                    <option value="{{ $kls->id }}">{{ $kls->nama_kelas }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="no_handphone">Nomor Handphone Siswa</label>
                            <input type="text" class="form-control" id="no_handphone"
                                placeholder="No Handphone Siswa" name="no_handphone">
                        </div>
                        <div class="form-group">
                            <label for="no_hp_orang_tua">Nomor Handphone Orang Tua</label>
                            <input type="text" class="form-control" id="no_hp_orang_tua"
                                placeholder="No Handphone Orang Tua" name="no_hp_orang_tua">
                        </div>
                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <textarea class="form-control" id="alamat" rows="4" name="alamat" placeholder="Alamat Siswa"></textarea>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan Data Siswa</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="{{ asset('vendors/mdi/css/materialdesignicons.min.css') }}">
@endpush

@push('scripts')
    <script src="https://code.jquery.com/jquery-3.7.1.js"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script> --}}
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/js/bootstrap.min.js"></script> --}}
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.bootstrap4.js"></script>
    <script src="{{ asset('js/swal.min.js') }}"></script>

    <script>
        new DataTable('#example');
    </script>

    @if (session('success'))
        <script>
            Swal.fire({
                title: 'Berhasil!',
                text: "{{ session('success') }}",
                icon: 'success',
                confirmButtonText: 'Ok'
            });
        </script>
    @endif

    @if (session('error'))
        <script>
            Swal.fire({
                title: 'Gagal!',
                text: "{{ session('error') }}",
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        </script>
    @endif

    @error('nama_kelas')
        <script>
            Swal.fire({
                title: 'Gagal!',
                text: "{{ $message }}",
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        </script>
    @enderror
@endpush
