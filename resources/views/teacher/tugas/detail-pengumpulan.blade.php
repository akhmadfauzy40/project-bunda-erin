@extends('layout.welcome')

@section('title')
    Detail Pengumpulan Tugas
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="row mb-3">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center">
                            <h5 class="card-title">{{ $pengumpulan->tugas->title }}</h5>
                        </div>
                        <p class="font-weight-500">{{ $pengumpulan->tugas->deskripsi }}</p>
                        @if ($pengumpulan->tugas->path_file != '')
                            <p class="font-weight-500"><a
                                    href="{{ asset('storage/tugas_files/' . $pengumpulan->tugas->path_file) }}"
                                    target="_blank">Klik Disini</a> untuk mendownload
                                soal</p>
                        @else
                            <p class="font-weight-500">Kerjakanlah Soal Dibawah Ini</p>
                            {!! $pengumpulan->tugas->soal !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Detail Jawaban Milik {{ $pengumpulan->user->name }}</h5>
                        @if (isset($pengumpulan->jawab_essay) && $pengumpulan->jawab_essay != '')
                            <p class="font-weight-500">Jawaban : </p>
                            {!! $pengumpulan->jawab_essay !!}
                        @elseif (isset($pengumpulan->path_file) && $pengumpulan->path_file != '')
                            <p class="font-weight-500"><a
                                    href="{{ asset('storage/tugas_files/' . $pengumpulan->path_file) }}"
                                    target="_blank">Klik Disini</a> untuk mendownload jawaban milik
                                {{ $pengumpulan->user->name }}</p>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">Tanggal & waktu Pengumpulan</th>
                                        <th scope="col">Nilai</th>
                                        <th scope="col">Keterangan Guru</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            @if ($pengumpulan)
                                                @php
                                                    $deadline =
                                                        $pengumpulan->tugas->deadline_tanggal .
                                                        ' ' .
                                                        $pengumpulan->tugas->deadline_jam;
                                                @endphp

                                                @if ($pengumpulan->updated_at > $deadline)
                                                    <div class="badge badge-danger">{{ $pengumpulan->updated_at }}</div>
                                                @else
                                                    <div class="badge badge-success">{{ $pengumpulan->updated_at }}</div>
                                                @endif
                                            @else
                                                <div class="badge badge-warning">Belum Mengumpulkan</div>
                                            @endif
                                        </td>
                                        <td>
                                            @if (isset($pengumpulan->nilai))
                                                {{ $pengumpulan->nilai }}
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>
                                            @if (isset($pengumpulan->catatan_guru))
                                                {{ $pengumpulan->catatan_guru }}
                                            @else
                                                -
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        @if ($pengumpulan && $pengumpulan->created_at)
                            @if (isset($pengumpulan->nilai))
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#exampleModal">
                                    Update Nilai
                                </button>
                            @else
                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                    data-target="#exampleModal">
                                    Beri Penilaian
                                </button>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Beri Penilaian Pada Tugas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/teacher/submit-nilai/{{ $pengumpulan->id }}" method="post">
                    @csrf
                    @method('put')
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="nilai">Nilai</label>
                            <input type="number" name="nilai" id="nilai"
                                class="form-control @error('nilai') is-invalid @enderror" max="100" min="0">
                            @error('nilai')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="catatan_guru">Catatan Guru</label>
                            <textarea class="form-control @error('catatan_guru') is-invalid @enderror" id="catatan_guru" rows="4"
                                name="catatan_guru"></textarea>
                            @error('catatan_guru')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit Nilai</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="{{ asset('vendors/mdi/css/materialdesignicons.min.css') }}">
    <script src="https://cdn.tiny.cloud/1/5gorw5nx4zw5j4viyd3rjucdwe1xqqwublsayv3cd879rzso/tinymce/7/tinymce.min.js"
        referrerpolicy="origin"></script>
@endpush

@push('scripts')
    <script src="https://code.jquery.com/jquery-3.7.1.js"></script>
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.bootstrap4.js"></script>
    <script src="{{ asset('js/swal.min.js') }}"></script>
    <script src="{{ asset('js/file-upload.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('.clickable-card').click(function() {
                // Get the URL from data-url attribute of clicked card
                var url = $(this).data('url');

                // Redirect to the specified URL
                window.location.href = url;
            });
        });
    </script>

    <script>
        new DataTable('#example');
    </script>

    @if (session('success'))
        <script>
            Swal.fire({
                title: 'Berhasil!',
                text: "{{ session('success') }}",
                icon: 'success',
                confirmButtonText: 'Ok'
            });
        </script>
    @endif

    @if (session('error'))
        <script>
            Swal.fire({
                title: 'Gagal!',
                text: "{{ session('error') }}",
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        </script>
    @endif

    @error('file')
        <script>
            Swal.fire({
                title: 'Gagal!',
                text: "{{ $message }}",
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        </script>
    @enderror
@endpush
