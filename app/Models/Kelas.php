<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    use HasFactory;

    protected $table = 'kelas';
    protected $fillable = ['nama_kelas', 'keterangan'];

    public function user()
    {
        return $this->hasMany(User::class, 'kelas_id', 'id');
    }

    public function topik()
    {
        return $this->hasMany(Topik::class, 'kelas_id', 'id');
    }

    public function tugas()
    {
        return $this->hasMany(Tugas::class, 'kelas_id', 'id');
    }
}
