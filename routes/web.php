<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DiskusiController;
use App\Http\Controllers\ForumController;
use App\Http\Controllers\KelasController;
use App\Http\Controllers\SiswaController;
use App\Http\Controllers\StorageController;
use App\Http\Controllers\TopikController;
use App\Http\Controllers\TugasController;
use App\Http\Controllers\TugasSiswaController;
use App\Models\Topik;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/


// Route::get('/teacher/dashboard', function () {
//     return view('teacher.dashboard');
// })->name('teacher.dashboard')->middleware('role:teacher');

Route::group(['middleware' => 'role:teacher'], function () {
    Route::get('/teacher/dashboard', [DashboardController::class, 'teacher_dashboard'])->name('teacher-dashboard');

    Route::get('/teacher/profile', [AuthController::class, 'index']);
    Route::put('/teacher/profile/edit', [AuthController::class, 'update']);
    Route::put('/teacher/profile/editpassword', [AuthController::class, 'updatepassword']);

    Route::get('/teacher/manage-kelas', [KelasController::class, 'index']);
    Route::post('/teacher/add-kelas', [KelasController::class, 'store']);
    Route::put('/teacher/update-kelas/{id}', [KelasController::class, 'update']);
    Route::delete('/teacher/delete-kelas/{id}', [KelasController::class, 'delete']);

    Route::get('/teacher/manage-siswa', [SiswaController::class, 'index']);
    Route::post('/teacher/add-siswa', [SiswaController::class, 'store']);
    Route::put('/teacher/update-siswa/{id}', [SiswaController::class, 'update']);
    Route::delete('/teacher/delete-siswa/{id}', [SiswaController::class, 'destroy']);

    Route::get('/teacher/topik-diskusi', [TopikController::class, 'index']);
    Route::get('/teacher/list-diskusi/{id}', [TopikController::class, 'list_diskusi']);
    Route::post('/teacher/add-topik', [TopikController::class, 'store']);
    Route::get('/teacher/forum-diskusi/{id}', [DiskusiController::class, 'index']);
    Route::post('/teacher/send-comment', [DiskusiController::class, 'send_comment']);
    Route::put('/teacher/update-topik/{id}', [TopikController::class, 'update']);
    Route::delete('/teacher/delete-topik/{id}', [TopikController::class, 'destroy']);

    Route::get('/teacher/tugas', [TugasController::class, 'index']);
    Route::get('/teacher/list-tugas/{id}', [TugasController::class, 'list_tugas']);
    Route::post('/teacher/add-tugas', [TugasController::class, 'store']);
    Route::get('/teacher/detail-tugas/{id}', [TugasController::class, 'detail_tugas']);
    Route::put('/teacher/update-tugas/{id}', [TugasController::class, 'update']);
    Route::delete('/teacher/delete-tugas/{id}', [TugasController::class, 'destroy']);
    Route::get('/teacher/detail-pengumpulan/{id}', [TugasController::class, 'tugas_siswa']);
    Route::put('/teacher/submit-nilai/{id}', [TugasController::class, 'submit_nilai']);
});

Route::group(['middleware' => 'role:student'], function () {
    Route::get('/student/dashboard', [DashboardController::class, 'student_dashboard'])->name('student-dashboard');

    Route::get('/student/profile', [AuthController::class, 'index']);
    Route::put('/student/profile/edit', [AuthController::class, 'update']);
    Route::put('/student/profile/editpassword', [AuthController::class, 'updatepassword']);

    Route::get('/student/forum-diskusi', [ForumController::class, 'index']);
    Route::get('/student/forum-diskusi/{id}', [ForumController::class, 'forum_siswa']);
    Route::post('/student/send-comment', [ForumController::class, 'send_comment']);

    Route::get('/student/tugas', [TugasSiswaController::class, 'index']);
    Route::get('/student/detail-tugas/{id}', [TugasSiswaController::class, 'detail_tugas']);
    Route::post('/student/submit-tugas', [TugasSiswaController::class, 'store']);
    Route::put('/student/update-tugas/{id}', [TugasSiswaController::class, 'update']);
});

Route::get('/', function(){
    return view('login');
})->name('login');

Route::post('/login', [AuthController::class, 'login']);
Route::get('/logout', [AuthController::class, 'logout']);
Route::get('/storage-link', [StorageController::class, 'createStorageLink']);
