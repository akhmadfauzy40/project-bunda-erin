<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use App\Models\User;
use Illuminate\Http\Request;

class KelasController extends Controller
{
    public function index()
    {
        $data = Kelas::all();
        return view('teacher.kelas', ['data' => $data]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama_kelas' => 'required|unique:kelas',
        ]);

        $data = new Kelas();
        $data->nama_kelas = $request->nama_kelas;
        $data->keterangan = $request->keterangan;
        $data->save();
        return redirect()->back()->with('success', 'Data Kelas Berhasil Ditambahkan');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_kelas' => 'required|unique:kelas,nama_kelas,' . $id
        ]);

        $data = Kelas::find($id);
        $data->nama_kelas = $request->nama_kelas;
        $data->keterangan = $request->keterangan;
        $data->save();

        return redirect('/teacher/manage-kelas')->with('success', 'Data Kelas Berhasil Perbarui');
    }

    public function delete($id)
    {
        $data = Kelas::find($id);
        if(User::where('kelas_id', $id)->count() > 0)
        {
            return redirect('/teacher/manage-kelas')->with('error', 'Terdapat Siswa Pada Kelas Ini!!');
        }
        else
        {
            $data->delete();
            return redirect('/teacher/manage-kelas')->with('success', 'Data Kelas Berhasil Dihapus');
        }
    }
}
