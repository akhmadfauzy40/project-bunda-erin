<?php

namespace App\Http\Controllers;

use App\Models\Diskusi;
use App\Models\Kelas;
use App\Models\Topik;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ForumController extends Controller
{
    public function index()
    {
        $data = Kelas::find(Auth::user()->kelas_id);

        return view('student.forum-diskusi.index', ['data' => $data]);
    }

    public function forum_siswa($id)
    {
        $data = Topik::find($id);

        return view('student.forum-diskusi.forum-diskusi', ['data' => $data]);
    }

    public function send_comment(Request $request)
    {
        $request->validate([
            'comment' => 'required',
        ]);

        $data = new Diskusi();
        $data->comment = $request->comment;
        $data->topik_id = $request->topik_id;
        $data->user_id = Auth::id();
        $data->save();

        return redirect()->back()->with('success', 'Tanggapan Anda Sudah di Kirimkan');
    }
}
