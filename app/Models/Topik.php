<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Topik extends Model
{
    use HasFactory;

    protected $table = 'topik';
    protected $fillable = ['title', 'deskripsi', 'path_file', 'kelas_id'];

    public function kelas()
    {
        return $this->belongsTo(Kelas::class, 'kelas_id', 'id');
    }

    public function diskusi()
    {
        return $this->hasMany(Diskusi::class, 'topik_id', 'id');
    }
}
