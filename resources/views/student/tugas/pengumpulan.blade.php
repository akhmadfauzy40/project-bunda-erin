@extends('layout.welcome')

@section('title')
    Pengumpulan Tugas
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="row mb-3">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex justify-content-between align-items-center">
                            <h5 class="card-title">{{ $tugas->title }}</h5>
                        </div>
                        <p class="font-weight-500">{{ $tugas->deskripsi }}</p>
                        @if ($tugas->path_file != '')
                            <p class="font-weight-500"><a href="{{ asset('storage/tugas_files/' . $tugas->path_file) }}"
                                    target="_blank">Klik Disini</a> untuk mendownload
                                soal</p>
                        @else
                            <p class="font-weight-500">Kerjakanlah Soal Dibawah Ini</p>
                            {!! $tugas->soal !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @if (!isset($pengumpulan->created_at))
            <div class="row mb-3">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Jawab Tugas</h5>
                            <p class="font-weight-500">Silahkan Pilih Salah Satu Metode Pengumpulan Tugas (Pilih jawab
                                menggunakan file jika anda ingin menjawab dengan melakukan upload file, dan pilih jawab
                                manual
                                jika anda ingin menjawab secara manual)</p>
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item active" role="presentation">
                                    <button class="nav-link active" id="home-tab" data-toggle="tab"
                                        data-target="#jawab_file" type="button" role="tab" aria-controls="home"
                                        aria-selected="true">Jawab
                                        menggunakan file</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="profile-tab" data-toggle="tab" data-target="#jawab_manual"
                                        type="button" role="tab" aria-controls="profile" aria-selected="false">Jawab
                                        Manual</button>
                                </li>
                            </ul>
                            <form action="/student/submit-tugas" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="jawab_file" role="tabpanel"
                                        aria-labelledby="home-tab">
                                        <div class="form-group">
                                            <label>Upload File Jawaban</label>
                                            <input type="file" name="file" id="file"
                                                class="file-upload-default @error('file') is-invalid @enderror">
                                            <div class="input-group col-xs-12">
                                                <input type="text" class="form-control file-upload-info" disabled
                                                    placeholder="Upload File Tugas">
                                                <span class="input-group-append">
                                                    <button class="file-upload-browse btn btn-primary"
                                                        type="button">Upload</button>
                                                </span>
                                            </div>
                                            @error('file')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="jawab_manual" role="tabpanel"
                                        aria-labelledby="profile-tab">
                                        <div class="form-group">
                                            <label for="jawab_essay">Silahkan jawab pada kolom dibawah ini</label>
                                            <textarea class="form-control @error('jawab_essay') is-invalid @enderror" id="jawab_essay" rows="4"
                                                name="jawab_essay"></textarea>
                                            @error('jawab_essay')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <input type="hidden" name="tugas_id" value="{{ $tugas->id }}">
                                </div>
                                <button type="submit" class="btn btn-primary mt-3">Submit Tugas</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Status Pengumpulan Anda</h5>
                        @if (isset($pengumpulan->jawab_essay) && $pengumpulan->jawab_essay != '')
                            <p class="font-weight-500">Jawaban Anda</p>
                            {!! $pengumpulan->jawab_essay !!}
                        @elseif (isset($pengumpulan->path_file) && $pengumpulan->path_file != '')
                            <p class="font-weight-500">Jawaban Anda</p>
                            <p class="font-weight-500"><a
                                    href="{{ asset('storage/tugas_files/' . $pengumpulan->path_file) }}"
                                    target="_blank">Klik Disini</a> untuk mendownload jawaban anda</p>
                        @endif
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">Tanggal & waktu Pengumpulan</th>
                                        <th scope="col">Nilai</th>
                                        <th scope="col">Keterangan Guru</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            @if ($pengumpulan)
                                                @php
                                                    $deadline = $tugas->deadline_tanggal . ' ' . $tugas->deadline_jam;
                                                @endphp

                                                @if ($pengumpulan->updated_at > $deadline)
                                                    <div class="badge badge-danger">{{ $pengumpulan->updated_at }}</div>
                                                @else
                                                    <div class="badge badge-success">{{ $pengumpulan->updated_at }}</div>
                                                @endif
                                            @else
                                                <div class="badge badge-warning">Belum Mengumpulkan</div>
                                            @endif
                                        </td>
                                        <td>
                                            @if (isset($pengumpulan->nilai))
                                                {{ $pengumpulan->nilai }}
                                            @else
                                                -
                                            @endif
                                        </td>
                                        <td>
                                            @if (isset($pengumpulan->catatan_guru))
                                                {{ $pengumpulan->catatan_guru }}
                                            @else
                                                -
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        @if ($pengumpulan && $pengumpulan->created_at)
                            <button type="button" class="btn btn-primary {{ $pengumpulan->nilai ? 'disabled' : '' }}"
                                data-toggle="modal" data-target="{{ $pengumpulan->nilai ? '' : '#exampleModal' }}">
                                Edit Pengumpulan Anda
                            </button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    @if ($pengumpulan && $pengumpulan->created_at)
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Pengumpulan Tugas Anda</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item active" role="presentation">
                                <button class="nav-link active" id="home-tab" data-toggle="tab"
                                    data-target="#jawab_file" type="button" role="tab" aria-controls="home"
                                    aria-selected="true">Jawab
                                    menggunakan file</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="profile-tab" data-toggle="tab" data-target="#jawab_manual"
                                    type="button" role="tab" aria-controls="profile" aria-selected="false">Jawab
                                    Manual</button>
                            </li>
                        </ul>
                        <form action="/student/update-tugas/{{ $pengumpulan->id }}" method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="jawab_file" role="tabpanel"
                                    aria-labelledby="home-tab">
                                    <div class="form-group">
                                        <label>Upload File Jawaban</label>
                                        <input type="file" name="file" id="file"
                                            class="file-upload-default @error('file') is-invalid @enderror">
                                        <div class="input-group col-xs-12">
                                            <input type="text" class="form-control file-upload-info" disabled
                                                placeholder="Upload File Tugas">
                                            <span class="input-group-append">
                                                <button class="file-upload-browse btn btn-primary"
                                                    type="button">Upload</button>
                                            </span>
                                        </div>
                                        @error('file')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="jawab_manual" role="tabpanel"
                                    aria-labelledby="profile-tab">
                                    <div class="form-group">
                                        <label for="jawab_essay">Silahkan jawab pada kolom dibawah ini</label>
                                        <textarea class="form-control @error('jawab_essay') is-invalid @enderror" id="jawab_essay" rows="4"
                                            name="jawab_essay"></textarea>
                                        @error('jawab_essay')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    @endif

@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="{{ asset('vendors/mdi/css/materialdesignicons.min.css') }}">
    <script src="https://cdn.tiny.cloud/1/5gorw5nx4zw5j4viyd3rjucdwe1xqqwublsayv3cd879rzso/tinymce/7/tinymce.min.js"
        referrerpolicy="origin"></script>
@endpush

@push('scripts')
    <script src="https://code.jquery.com/jquery-3.7.1.js"></script>
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.js"></script>
    <script src="https://cdn.datatables.net/2.0.2/js/dataTables.bootstrap4.js"></script>
    <script src="{{ asset('js/swal.min.js') }}"></script>
    <script src="{{ asset('js/file-upload.js') }}"></script>

    <script>
        tinymce.init({
            selector: '#jawab_essay',
            plugins: 'advlist autolink autosave code colorpicker emoticons fullscreen hr image imagetools lists link media noneditable nonbreaking pagebreak paste preview print save searchreplace table textcolor textpattern toc visualblocks visualchars wordcount',
            toolbar: 'formatselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | outdent indent | numlist bullist | forecolor backcolor removeformat | link image media | code',
        });
    </script>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            const fileInput = document.querySelector('#file');
            const textareaInput = document.querySelector('#jawab_essay');
            const form = document.querySelector('form');

            // Mengatur event listener untuk form submit
            form.addEventListener('submit', function(event) {
                // Cek apakah keduanya kosong
                if (fileInput.files.length === 0 && textareaInput.value.trim() === '') {
                    Swal.fire({
                        title: 'Peringatan',
                        text: "Mohon salah satu jenis soalnya di isi",
                        icon: 'error',
                        confirmButtonText: 'Ok'
                    });
                    event.preventDefault(); // Menghentikan pengiriman form jika keduanya kosong
                }
            });

            // Mengatur event listener untuk input file
            fileInput.addEventListener('change', function() {
                textareaInput.value = ''; // Mengosongkan nilai textarea jika input file diisi
            });

            // Mengatur event listener untuk textarea
            textareaInput.addEventListener('input', function() {
                if (this.value.trim() !== '') {
                    fileInput.value = ''; // Mengosongkan nilai input file jika textarea diisi
                }
            });
        });
    </script>

    <script>
        $(document).ready(function() {
            $('.clickable-card').click(function() {
                // Get the URL from data-url attribute of clicked card
                var url = $(this).data('url');

                // Redirect to the specified URL
                window.location.href = url;
            });
        });
    </script>

    <script>
        new DataTable('#example');
    </script>

    @if (session('success'))
        <script>
            Swal.fire({
                title: 'Berhasil!',
                text: "{{ session('success') }}",
                icon: 'success',
                confirmButtonText: 'Ok'
            });
        </script>
    @endif

    @if (session('error'))
        <script>
            Swal.fire({
                title: 'Gagal!',
                text: "{{ session('error') }}",
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        </script>
    @endif

    @error('file')
        <script>
            Swal.fire({
                title: 'Gagal!',
                text: "{{ $message }}",
                icon: 'error',
                confirmButtonText: 'Ok'
            });
        </script>
    @enderror
@endpush
